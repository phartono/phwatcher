/*
* Reference: https://thisdavej.com/using-winston-a-versatile-logging-library-for-node-js/
*/

const { createLogger, format, transports } = require('winston');
const path = require('path');

function CustomLog() 
{
  self = this; // Will come in handy

  this.create = function(env, logFile = '') 
  {
    if (logFile == null || logFile == '' || logFile == 'undefined')
    {
      return createLogger({
        // change level if in dev environment versus production
        level: (env === 'prod' || env === 'production') ? 'info' : 'debug',
        format: format.combine(
          format.label({ label: path.basename(process.mainModule.filename) }),
          format.timestamp({ format: 'YYYY-MM-DD HH:mm:ss' }),
          format.splat()
        ),
        transports: [
          new transports.Console({
            format: format.combine(
                format.colorize(),
                format.printf(
                info =>
                    `${info.timestamp} ${info.level} [${info.label}]: ${info.message}`
                )
            )
          })
        ]
      });
    }
    else
    {
      return createLogger({
        // change level if in dev environment versus production
        level: (env === 'prod' || env === 'production') ? 'info' : 'debug',
        format: format.combine(
          format.label({ label: path.basename(process.mainModule.filename) }),
          format.timestamp({ format: 'YYYY-MM-DD HH:mm:ss' })
        ),
        transports: [
          new transports.Console({
            format: format.combine(
                format.colorize(),
                format.printf(
                info =>
                    `${info.timestamp} ${info.level} [${info.label}]: ${info.message}`
                )
            )
          }),
          
          new transports.File({
            filename: logFile,
            format: format.combine(
                format.printf(
                info =>
                    `${info.timestamp} ${info.level} [${info.label}]: ${info.message}`
                )
            )
          })
        ]
      });
    }
  }
}

module.exports = CustomLog;