var path = require('path');

const argv = require('yargs')
  .usage('Usage: $0 $1 <patterns> [options]')
  .command('<patterns>', '(string or array of strings). Paths to files, dirs to be watched recursively, or glob patterns. Reference: https://www.npmjs.com/package/chokidar')
  .describe('env', 'Load environment').alias('e', 'env').choices('e', ['dev','stag','prod']).default('env','dev')
  .describe('configpath', 'Load Custom Environment Config Path').alias('c', 'configpath')
  .describe('directory', 'Base directory path to watch').alias('d', 'directory')
  .describe('polling', 'Use Polling or not. Default is false').alias('p', 'polling')
  .describe('event', 'FS Event to listen.').alias('t', 'event').choices('t',['add', 'addDir', 'change', 'unlink', 'unlinkDir', 'ready', 'raw', 'error', 'all'])
  .describe('plugin', 'Plugin workflow to use. Default is "callhttp" plugin')
  .describe('read', 'Do you want to read file content? (1 = Yes; 0 = No). Default is 1 (true)')
  .describe('success', 'Directory location to put the SUCCESSFUL OUTPUT')
  .describe('failed', 'Directory location to put the FAILED OUTPUT')
  .describe('support_phone', 'Support Phone Number')
  .describe('support_email', 'Support Email Address')
  .describe('logfile', 'Log File location (default stdout)')
  .help('h').alias('h', 'help')
  .example('phwatcher -e prod', 'Load "prod" environment using env defined in prod.env')
  .example('')
  .example('phwatcher --success "success_dir" --failed "failed_dir', 'Define success and failed OUTPUT directory')
  .epilog('Copyright 2019 - Paul Hartono')
  .showHelpOnFail(true)
  .argv;

let env = process.env.NODE_ENV || argv.env || 'dev'; // 'dev' or 'stag' or 'prod'

if (!process.env.NODE_ENV) {
  if (!argv.configpath) {
  
    if (argv.env) {
      argv.configpath = path.join (__dirname, 'env', argv.env + '.env');
    } else {
      // this is default value from dotenv
      // https://github.com/motdotla/dotenv#readme
      argv.configpath = path.resolve(process.cwd(), '.env'); 
    }
  
    require('dotenv').config({ path: argv.configpath })
  } else {
    require('dotenv').config({ path: argv.configpath })
    env = process.env.NODE_ENV || argv.env || 'dev'; // 'dev' or 'stag' or 'prod'
  }  
}

console.log(argv._[0]);

const dev = {
 
  logFile: process.env.LOG_FILE || argv.logfile || '',

  support: {
    phone: process.env.SUPPORT_PHONE || argv.support_phone || '',
    email: process.env.SUPPORT_EMAIL || argv.support_email || ''
  },

  plugin: process.env.PLUGIN || argv.plugin || '',
  directory: process.env.DIRECTORY || argv.directory || '.',
  patterns: process.env.PATTERNS || argv._[0] || '*',
  polling: process.env.POLLING || argv.polling || false,
  event: process.env.EVENT || argv.event || 'all',
  isReadFileContent: process.env.READ_FILE_CONTENT || argv.read || true,
  successOutputDir: process.env.SUCCESS_OUT_DIR || argv.success || '',
  failedOutputDir: process.env.FAILED_OUT_DIR || argv.failed || '',
};

const stag = {
 
  logFile: process.env.LOG_FILE || argv.logfile || '',

  support: {
    phone: process.env.SUPPORT_PHONE || argv.support_phone || '',
    email: process.env.SUPPORT_EMAIL || argv.support_email || ''
  },

  plugin: process.env.PLUGIN || argv.plugin || '',
  directory: process.env.DIRECTORY || argv.directory || '.',
  patterns: process.env.PATTERNS || argv._[0] || '*',
  polling: process.env.POLLING || argv.polling || false,
  event: process.env.EVENT || argv.event || 'all',
  isReadFileContent: process.env.READ_FILE_CONTENT || argv.read || true,
  successOutputDir: process.env.SUCCESS_OUT_DIR || argv.success || '',
  failedOutputDir: process.env.FAILED_OUT_DIR || argv.failed || '',
};

const prod = {
  
  logFile: process.env.LOG_FILE || argv.logfile || '',
 
  support: {
    phone: process.env.SUPPORT_PHONE || argv.support_phone || '',
    email: process.env.SUPPORT_EMAIL || argv.support_email || ''
  },

  plugin: process.env.PLUGIN || argv.plugin || '',
  directory: process.env.DIRECTORY || argv.directory || '.',
  patterns: process.env.PATTERNS || argv._[0] || '*',
  polling: process.env.POLLING || argv.polling || false,
  event: process.env.EVENT || argv.event || 'all',
  isReadFileContent: process.env.READ_FILE_CONTENT || argv.read || true,
  successOutputDir: process.env.SUCCESS_OUT_DIR || argv.success || '',
  failedOutputDir: process.env.FAILED_OUT_DIR || argv.failed || '',
};

const config = {
  dev,
  stag,
  prod
};
   

module.exports = config[env];

module.exports.env = env;

module.exports.argv = argv;