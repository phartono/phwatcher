# Introduction

Hate of a cron jobs process to monitor file in a hot folder which runs periodically? **phwatcher** comes to the rescue!
**phwatcher** is a CLI filesystem event watcher. It is flexible enough and easy to extend! 


# Getting Started

1. Install [Node.js](https://nodejs.org/en/)

2. Go to your project dir using command line and Type `npm link` to install and link necessary dependencies

3. Type `phwatcher -h` to see the help command, and there you go!

## Usage
`phwatcher [patterns] [options]`
When **phwatcher** trigger an event, it will pass the filepath and the file content (if the `read` option is set) to the plugin. It is up to the plugin to process what happen next!

#### Help (-h)
**Example:** `phwatcher -h`
This command will display the help functions.
```
PS C:\Project\phwatcher> phwatcher -h
Usage: app.js $1 <patterns> [options]

Commands:
  app.js <patterns>  (string or array of strings). Paths to files, dirs to be
                     watched recursively, or glob patterns. Reference:
                     https://www.npmjs.com/package/chokidar

Options:
  --version         Show version number                                [boolean]
  --plugin          Plugin workflow to use
  --read            Do you want to read file content? (1 = Yes; 0 = No)
  --success         Directory location to put the SUCCESSFUL OUTPUT
  --failed          Directory location to put the FAILED OUTPUT
  --support_phone   Support Phone Number
  --support_email   Support Email Address
  --logfile         Log File location (default stdout)
  -h, --help        Show help                                          [boolean]
  -e, --env         Load environment
                               [choices: "dev", "stag", "prod"] [default: "dev"]
  -c, --configpath  Load Custom Environment Config Path
  -d, --directory   Base directory path to watch
  -p, --polling     Use Polling or not
  -t, --event       FS Event to listen.
     [choices: "add", "addDir", "change", "unlink", "unlinkDir", "ready", "raw",
                                                                 "error", "all"]

Examples:
  app.js -e prod                            Load "prod" environment

  app.js -q "amqp://localhost" -n "datazoo  Define message queue host URI and
                                            name to listen to

Copyright 2019 - Paul Hartono

Commands:
  app.js <patterns>  (string or array of strings). Paths to files, dirs to be
                     watched recursively, or glob patterns. Reference:
                     https://www.npmjs.com/package/chokidar

Options:
  --version         Show version number                                [boolean]
  --plugin          Plugin workflow to use
  --read            Do you want to read file content? (1 = Yes; 0 = No)
  --success         Directory location to put the SUCCESSFUL OUTPUT
  --failed          Directory location to put the FAILED OUTPUT
  --support_phone   Support Phone Number
  --support_email   Support Email Address
  --logfile         Log File location (default stdout)
  -h, --help        Show help                                          [boolean]
  -e, --env         Load environment
                               [choices: "dev", "stag", "prod"] [default: "dev"]
  -c, --configpath  Load Custom Environment Config Path
  -d, --directory   Base directory path to watch
  -p, --polling     Use Polling or not
  -t, --event       FS Event to listen.
     [choices: "add", "addDir", "change", "unlink", "unlinkDir", "ready", "raw",
                                                                 "error", "all"]

Examples:
  app.js -e prod                            Load "prod" environment

  app.js -q "amqp://localhost" -n "datazoo  Define message queue host URI and
                                            name to listen to

Copyright 2019 - Paul Hartono
```

#### Patterns
This can be Paths to files, or dirs, or **glob** patterns.

**Example:** 
```
phwatcher test/**
```
will watch any file added inside the `test` folder recursiveley 

## Environment Variables
Rather than typing all the parameter argument in CLI, you may want to set everything inside the environment variable files.
Here are all the available variable that are available:

**PLUGIN** = Plugin workflow to use. Default is "callhttp" plugin'. 

**DIRECTORY** = Base directory path to watch. 

**PATTERNS** = This can be Paths to files, or dirs, or **glob** patterns. 

**POLLING** = Use Polling or not. Default is false. 

**EVENT** = File System Event to listen. choices: 'add', 'addDir', 'change', 'unlink', 'unlinkDir', 'ready', 'raw', 'error', 'all'. 

**READ_FILE_CONTENT** = Do you want to read file content? (1 = Yes; 0 = No). Default is 1 (true). 

**SUCCESS_OUT_DIR** = Directory location to put the SUCCESSFUL OUTPUT. 

**FAILED_OUT_DIR** = Directory location to put the FAILED OUTPUT. 

There are 2 ways to define environment variables:
### 1. Predefined environment variable files
**dev, stag, and prod** - are the predefined keyword - and you can specify this using the --env=[variable] command or simply with -e [variable]. When called it will load configuration inside `env/[variable].env`.
If you type `phwatcher -e prod`, then `env/prod.env` will be loaded

### 2. Custom Environment variable files
You can setup your own environment variable file and save it to any location, and call the CLI using the `--configpath=[path to env file]` or `-c [path to env file]`

## Plugin
*phwatcher* can be extended by writing a plugin. Plugin MUST be put inside the `plugin/` directory and called by the filename without .js extension. As of current *phwatcher* has 1 plugin that is **callhttp**. 

#### callhttp
This plugin would require `--read=1` when calling through CLI or `READ_FILE_CONTENT = true` to be defined in environment variable. this plugin would simply call (and assume) the content of the file is a url.




