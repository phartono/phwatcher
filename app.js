#!/usr/bin/env node
const config = require('./config');
const pkginfo = require('pkginfo')(module);

const CustomLog = require('./lib/custom-log');
const logger = new CustomLog().create(config.env, config.logFile);

const chokidar = require('chokidar');

var events = require('events');
var eventEmitter = new events.EventEmitter();

var fs = require('fs');
var path = require('path');

global.pluginFile = null;
global.pluginInstance = null;


eventEmitter.on('moveProcessedFile', function(plugin, filepath, error) {
  try {

    var outputDir = '';
    if (error) { 
      outputDir = config.failedOutputDir; 
    }
    else { 
      outputDir = config.successOutputDir;
    }

    var mkdirp = require('mkdirp');
    mkdirp(outputDir, function(err) { 

      if (err) {
        logger.error('Failed to Create DIR -> ' + outputDir);
        console.error(err);
        process.exit(1);
      }

      // path exists unless there was an error
      logger.debug('Moving ' + filepath + ' to OUTPUT DIR -> ' + outputDir);

      var baseFilename = path.basename(filepath);
      var outputPath = path.join(outputDir, baseFilename)

      fs.rename(filepath, outputPath, (err) => {
        if (err) 
        {
          logger.error('Failed to move from [' + filepath + '] TO [' + outputPath + ']');
          console.error(err);
          //process.exit(1);
        }
        else
        {
          if (error) 
          { 
            logger.error('File Moved to ERROR DIR [' + outputPath + ']');
          }
          else
          {
            logger.info('File Moved to SUCCESS DIR [' + outputPath + ']');
          }
        }
      });
    });
  } catch (error) {
    logger.error("[EXCEPTION] on 'moveProcessedFile' event");
    console.error(error);
    process.exit(1);
  }
});

eventEmitter.on('callPluginCompleted', function(plugin, filepath, error) {
  eventEmitter.emit('moveProcessedFile', plugin, filepath, error);
});

eventEmitter.on('callPlugin', function(plugin, filepath, content) {
  try {
    plugin.process(filepath, content, function(response, error) {
      if (error) {
        logger.error('Plugin Error: ' + error.type + ' | ' + error.msg);
      }

      logger.info('Response: ');
      logger.info('--------------------- ');
      console.log(response);
      logger.info('--------------------- ');
      
      // do something with the response if needed here for future
      eventEmitter.emit('callPluginCompleted', plugin, filepath, error);

    });
  } catch (error) {
    logger.error("[EXCEPTION] on 'callPlugin' event");
    console.error(error);
    process.exit(1);
  }
});

eventEmitter.on('readFileContentCompleted', function(plugin, filepath, content) {
  eventEmitter.emit('callPlugin', plugin, filepath, content);
});

eventEmitter.on('readFileContent', function(plugin, filepath) {
  try {

    fs.readFile(filepath, {encoding: 'utf-8'}, function(err,content) {
      if (!err) {
        logger.info('Received Content: ' + content);
        eventEmitter.emit('readFileContentCompleted', plugin, filepath, content);
      } else {
        logger.error('error while reading file content [' + filepath +']: ' + err);
      }
    });
  } catch(error) {
    logger.error("[EXCEPTION] on 'readFileContent' event");
    console.error(error);
    process.exit(1);
  }
});

eventEmitter.on('watchFileSystemCompleted', function(plugin, filepath) {
  if (config.isReadFileContent) 
  {
    eventEmitter.emit('readFileContent', plugin, filepath);
  }
  else
  {
    eventEmitter.emit('callPlugin', plugin, filepath, null);
  }
});

eventEmitter.on('watchFileSystem', function(plugin) {
  try {

    // Initialize watcher.
    const watcher = chokidar.watch(config.patterns, {
      ignored: /(^|[\/\\])\../, // ignores .dotfiles
      cwd: config.directory,
      usePolling: config.polling
    });

    watcher.on(config.event, (var1, var2, var3) => {
      console.log();
      logger.info('FS event info: ' + var1 + ', ' +  var2 + ', ' + var3 );
      
      var file = (typeof var2 == 'string') ? var2 : var1;
      var filepath = path.join(config.directory, file);

      eventEmitter.emit('watchFileSystemCompleted', plugin, filepath);      
    })
    .on('error', error => {
      logger.error('Watcher Error: ' + error); 
    });
    
 
  } catch(error) {
    logger.error("[EXCEPTION] on 'watchFileSystem' event");
    console.error(error);
    process.exit(1);
  }
});

eventEmitter.on('loadPluginCompleted', function(plugin) {
  eventEmitter.emit('watchFileSystem', plugin);
});

eventEmitter.on('loadPlugin', function() {
  try {

    if (!config.plugin) {
      logger.error('[ERROR] --plugin value is undefined. Please either set through PLUGIN environment variable or the --plugin argument');
      logger.error('   -> see --help for more command line syntax');
      logger.error();
      process.exit(1);
    }

    var pluginFile = config.plugin;
    plugin = new (require('./plugins/' + pluginFile))(config);

    logger.info('[x] Plugin is loaded (' + pluginFile + ').');
    
    global.pluginFile = pluginFile;
    global.pluginInstance = plugin;

    eventEmitter.emit('loadPluginCompleted', plugin);
  } catch(error) {
    logger.error("[EXCEPTION] on 'loadPlugin' event");
    console.error(error);
    process.exit(1);
  }
});

logger.info("=====================================================");
logger.info("==> " + module.exports.name + " started [version: " + module.exports.version + "]");
logger.info("=====================================================");
logger.info("  argv.configpath : " + config.argv.configpath);
logger.info();
logger.info("  env : [" + config.env + "]");
logger.info();
logger.info("  plugin: " + config.plugin);
logger.info("  patterns: " + config.patterns);
logger.info("  usePolling: " + config.polling);
logger.info("  event: " + config.event);
logger.info("  directory: " + config.directory);
logger.info("  successOutputDir: " + config.successOutputDir);
logger.info("  failedOutputDir: " + config.failedOutputDir);
logger.info("  support.email: " + config.support.email);
logger.info("=====================================================");
logger.info();

eventEmitter.emit('loadPlugin');