const CustomLog = require('../lib/custom-log');
var request = require("request");

function CallHttp(config) {

  this.config = config;
  this.logger = new CustomLog().create(config.env, config.logFile);
  self = this; // Will come in handy

  // MUST BE IMPLEMENTED!
  this.process = function(filepath, content, callback) {
    
    try 
    {
      self.logger.info('Calling URL -> ' + content);

      var options = { 
        method: 'GET',
        url: content,
        headers: 
        { 'cache-control': 'no-cache',
          'Content-Type': 'application/json' } 
      };

      request(options, function (error, response, body) {
        if (error) {
          return callback(body, {type: 'httpError', msg: error});
        }

        if (response.statusCode >= 400) 
        {
          return callback(body, {type: 'httpResponseFailure', msg: response.statusCode});          
        } 
        else
        {
          return callback(body, null);
        }
        
      });
    } 
    catch (error) 
    {
      return callback(null, {type: 'exception', msg: error});
    }
  }
}

module.exports = CallHttp;